import argparse
import json
import os
from ollama import generate
from srt2json import read_srt, parse_srt, replace_captions


def get_chunks(keys, chunk_size):
    return [keys[i:i + chunk_size] for i in range(0, len(keys), chunk_size)]


def validate_json(json_str, expected_keys):
    try:
        data = json.loads(json_str)
        return len(data) == expected_keys
    except json.JSONDecodeError:
        return False


def load_input(filename):
    data = {}
    if filename.endswith('.srt'):
        data = parse_srt(read_srt(filename))

    if filename.endswith('.json'):
        with open(filename, 'r', encoding='utf-8') as jsonfile:
            data = json.load(jsonfile)

    return data


def write_output(filename, data):
    with open(filename, 'w', encoding='utf-8') as outfile:
        outfile.write(data)


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Translate SRT/JSON file in chunks.'
    )
    parser.add_argument('-i', '--input', required=True,
                        help='Input JSON file')
    parser.add_argument('-o', '--output', required=True,
                        help='Output JSON file')
    parser.add_argument('-l', '--language', required=True,
                        help='Target language for translation')
    parser.add_argument('-c', '--chunk', required=False,
                        help='Chunk Size for the context',
                        type=int,
                        default=20)
    parser.add_argument('-m', '--model', required=False,
                        help='LLM for translation',
                        default='llama3:8b')
    parser.add_argument('-t', '--temperature', required=False,
                        help='Default LLM temperature for inference',
                        type=float,
                        default=0)
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Enable verbose output')
    return parser.parse_args()


def translate(model, prompt, options, system):
    response = ""
    for part in generate(model,
                         prompt=prompt,
                         options=options,
                         system=system,
                         stream=True,
                         format='json'):
        print(part['response'], end='', flush=True)
        response += part['response']

    return response


def system_template():
    return (
"""You are a machine that only returns and replies with valid, iterable RFC8259 compliant JSON in your responses
JSON consist of KEY-VALUE pairs.
KEY is NUMBER.
VALUE is TEXT STRING.
Translate the text in VALUE exactly as is to {language} language.
You MUST NOT remove meaningful infromation from the VALUE text.
Do NOT translate any text that is already in {language} language and copy it as is.
Do NOT translate any special constructions and copy them as is.
Do NOT use transliteration.
Do NOT translate REFERENCE
Use REFERENCE only for contextual meaning.
REFERENCE:
```
{reference}
```
Translate only following:
"""
    )


def process_chunks(chunks,
                   data,
                   language,
                   model,
                   options,
                   default_temperature,
                   verbose):
    translated_data = {}
    for chunk in chunks:
        chunk_data = {key: data[key] for key in chunk}
        reference = json.dumps(chunk_data, indent=0, ensure_ascii=False)
        system = system_template().format(
            language=language,
            reference=reference
        )

        tmp_data = process_chunk(chunk_data,
                                 verbose,
                                 system,
                                 model,
                                 options,
                                 default_temperature)
        translated_data.update(tmp_data)
    return translated_data


def process_chunk(chunk_data,
                  verbose,
                  system,
                  model,
                  options,
                  default_temperature):
    tmp_data = {}
    for key in chunk_data:
        prompt = json.dumps(
            {key: chunk_data[key]},
            indent=0,
            ensure_ascii=False
        )
        if verbose:
            print("System:", system)
            print("Prompt:", prompt)

        valid_response = False
        while not valid_response:
            response = translate(model, prompt, options, system)
            if validate_json(response, 1):
                valid_response = True
                options['temperature'] = default_temperature
            else:
                options['temperature'] = default_temperature + 0.3

        tmp_data.update(json.loads(response))
    return tmp_data


def main():
    args = parse_arguments()
    chunk_size = args.chunk
    options = {'temperature': args.temperature}
    data = load_input(args.input)
    chunks = get_chunks(list(data.keys()), chunk_size)
    response = ""

    translated_data = process_chunks(chunks,
                                     data,
                                     args.language,
                                     args.model,
                                     options,
                                     args.temperature,
                                     args.verbose)

    if not translated_data:
        print("Output is empty")
    else:
        if args.output.endswith('.json'):
            json_data = json.dumps(
                translated_data,
                ensure_ascii=False,
                indent=0
            )
            write_output(args.output, json_data)
        if args.output.endswith('.srt'):
            srt_data = read_srt(args.input)
            new_srt_data = replace_captions(srt_data, translated_data)
            write_output(args.output, new_srt_data)


if __name__ == "__main__":
    main()
