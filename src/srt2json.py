import sys
import json
import re


def parse_srt(content):
    pattern = timestamp_pattern()
    matches = pattern.findall(content)
    captions = {
        int(match[0]): match[2].replace('\n', ' ') for match in matches
    }
    return captions


def read_srt(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        content = file.read() + "\n\n"
    return content


def replace_captions(content, new_captions):
    pattern = timestamp_pattern()
    return pattern.sub(lambda match: replacer(match, new_captions), content)


def replacer(match, new_captions):
    caption_number = int(match.group(1))
    if str(caption_number) in new_captions:
        return (
            f"{match.group(1)}\n"
            f"{match.group(2)}\n"
            f"{new_captions[str(caption_number)]}\n\n"
        )
    else:
        return match.group(0)


def timestamp_pattern():
    pattern = re.compile(
        (
            r'(\d+)\n(\d{2}:\d{2}:\d{2},\d{3} --> '
            r'\d{2}:\d{2}:\d{2},\d{3})\n(.*?)\n\n'
        ),
        re.DOTALL
    )

    return pattern


def main():
    if len(sys.argv) < 2:
        print("Usage: script.py <input_srt> [<input_json>]")
        return

    input_srt = sys.argv[1]
    srt_content = read_srt(input_srt)
    captions = parse_srt(srt_content)

    if len(sys.argv) == 2:
        print(json.dumps(captions, ensure_ascii=False, indent=4))
    elif len(sys.argv) == 3:
        input_json = sys.argv[2]
        with open(input_json, 'r', encoding='utf-8') as file:
            new_captions = json.load(file)
        new_srt_content = replace_captions(srt_content, new_captions)
        print(new_srt_content)


if __name__ == "__main__":
    main()
