# Kraftstream Subtitle Translator
Experimental context aware Subtitle translator.
Uses llama3 LLM for translation.

## Launch 🚀
Start docker container:
```
docker compose up
```

Open docker container:
```
docker compose exec gitlab-gpu bash
```

Start translator:
```
python3 src/translate.py -i srt_samples/dialog-ru.srt -o test.srt -l english
```

### docker compose profiles
Starting with optionals profiles:
```
docker compose --profile XXXXX up
```
- `gitlab-gpu` (default) - download image from GitLab with Nvidia GPU support;
- `gitlab-cpu` - download image from Gitlab in CPU-only mode;
- `local-gpu` - build Dockerfile and start with Nvidia GPU support;
- `local-cpu` - build Dockerfile and start in CPU-only mode;

### (optional) without Docker
Create virtual environment and install dependencies:
```
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
```

Install Ollama: https://ollama.com/download

Start Ollama:
```
ollama serve
```

Download LLM:
```
ollama pull llama3:8b
```
