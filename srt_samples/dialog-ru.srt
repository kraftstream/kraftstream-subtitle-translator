1
00:00:00,000 --> 00:00:05,000
Привет, как дела?

2
00:00:05,000 --> 00:00:10,000
Привет! У меня все хорошо, а у тебя?

3
00:00:10,000 --> 00:00:15,000
У меня тоже все хорошо. Чем занимался на выходных?

4
00:00:15,000 --> 00:00:20,000
Мы с друзьями ходили в кино.

5
00:00:20,000 --> 00:00:25,000
Правда? Какой фильм смотрели?

6
00:00:25,000 --> 00:00:30,000
Мы смотрели новый экшн-фильм. Было очень интересно.

7
00:00:30,000 --> 00:00:35,000
Звучит здорово! Я давно хотел его посмотреть.

8
00:00:35,000 --> 00:00:40,000
Советую тебе обязательно его посмотреть.

9
00:00:40,000 --> 00:00:45,000
Спасибо за совет! А что еще нового?

10
00:00:45,000 --> 00:00:50,000
На следующей неделе мы собираемся поехать на море.

11
00:00:50,000 --> 00:00:55,000
Отличная идея! Надеюсь, вы получите много удовольствия.

12
00:00:55,000 --> 00:01:00,000
Да, мы тоже так надеемся.

13
00:01:00,000 --> 00:01:05,000
Ладно, мне пора идти. До встречи!

14
00:01:05,000 --> 00:01:10,000
До встречи! Удачного дня!

15
00:01:10,000 --> 00:01:15,000
Спасибо, и тебе тоже!

16
00:01:15,000 --> 00:01:20,000
Так что ты думаешь о новом проекте?

17
00:01:20,000 --> 00:01:25,000
Мне кажется, он очень перспективный.

18
00:01:25,000 --> 00:01:30,000
Согласен, я думаю, что он будет успешен.

19
00:01:30,000 --> 00:01:35,000
Как думаешь, когда сможем его запустить?

20
00:01:35,000 --> 00:01:40,000
Возможно, через месяц, если все пойдет по плану.

21
00:01:40,000 --> 00:01:45,000
Хорошо, давай тогда подготовимся и начнем работу.

22
00:01:45,000 --> 00:01:50,000
Давай, так и сделаем.

23
00:01:50,000 --> 00:01:55,000
Кстати, ты слышал о наших новых партнёрах?

24
00:01:55,000 --> 00:02:00,000
Да, слышал. Думаю, это будет отличное сотрудничество.

25
00:02:00,000 --> 00:02:05,000
Согласен, я тоже так думаю.

26
00:02:05,000 --> 00:02:10,000
Ладно, мне нужно идти на встречу. До связи!

27
00:02:10,000 --> 00:02:15,000
Хорошо, удачной встречи!

28
00:02:15,000 --> 00:02:20,000
До свидания!

29
00:02:20,000 --> 00:02:25,000
Привет! Не ожидал тебя здесь увидеть.

30
00:02:25,000 --> 00:02:30,000
Привет! Я тоже не ожидал, что ты здесь будешь.

31
00:02:30,000 --> 00:02:35,000
Как дела?

32
00:02:35,000 --> 00:02:40,000
Все хорошо, спасибо. А у тебя как?

33
00:02:40,000 --> 00:02:45,000
Тоже хорошо. Как прошли выходные?

34
00:02:45,000 --> 00:02:50,000
Очень весело! Я ходил на концерт.

35
00:02:50,000 --> 00:02:55,000
Правда? Это должно было быть здорово!

36
00:02:55,000 --> 00:03:00,000
Да, концерт был потрясающе!

37
00:03:00,000 --> 00:03:05,000
Какой группой ты был?

38
00:03:05,000 --> 00:03:10,000
Мы пошли на концерт местной рок-группы.

39
00:03:10,000 --> 00:03:15,000
Здесь много талантливых групп.

40
00:03:15,000 --> 00:03:20,000
Согласен, планирую еще раз пойти на их концерт.

41
00:03:20,000 --> 00:03:25,000
Звучит отлично, повезло тебе!

42
00:03:25,000 --> 00:03:30,000
Спасибо! Надеюсь, мы еще увидимся. До встречи!

43
00:03:30,000 --> 00:03:35,000
До встречи!

44
00:03:35,000 --> 00:03:40,000
Как думаешь, нам стоит провести еще одну встречу по проекту?

45
00:03:40,000 --> 00:03:45,000
Думаю да, нужно обсудить некоторые детали.

46
00:03:45,000 --> 00:03:50,000
Согласен, давай назначим время на следующей неделе.

47
00:03:50,000 --> 00:03:55,000
Отлично, я подберу время и сообщу тебе.

48
00:03:55,000 --> 00:04:00,000
Хорошо, буду ждать.

49
00:04:00,000 --> 00:04:05,000
Как твой день прошел?

50
00:04:05,000 --> 00:04:10,000
Неплохо, было много встреч.

51
00:04:10,000 --> 00:04:15,000
Понимаю, у меня тоже.

52
00:04:15,000 --> 00:04:20,000
Такие дни бывают.

53
00:04:20,000 --> 00:04:25,000
Ладно, мне пора. До встречи!

54
00:04:25,000 --> 00:04:30,000
До встречи, хорошего вечера!

55
00:04:30,000 --> 00:04:35,000
И тебе!

56
00:04:35,000 --> 00:04:40,000
Привет! Как настроение?

57
00:04:40,000 --> 00:04:45,000
Привет! Настроение отличное, а у тебя?

58
00:04:45,000 --> 00:04:50,000
У меня тоже все хорошо, спасибо. Что нового?

59
00:04:50,000 --> 00:04:55,000
Все по-старому, работаю над проектом.

60
00:04:55,000 --> 00:05:00,000
Понял, удачи тебе в работе!
