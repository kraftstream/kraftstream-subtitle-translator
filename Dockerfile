FROM ollama/ollama:0.1.48

WORKDIR /app

RUN ollama serve & sleep 5; ollama pull llama3:8b; \
    echo "kill 'ollama serve' process"; \
    ps -ef | grep 'ollama serve' | grep -v grep | awk '{print $2}' | xargs -r kill -9
RUN apt-get update; apt-get install -y python3-pip

COPY requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY src /app/src
